# Que puedo encontrar en este sitio?

Aqui encontraras una guia rapida para instalar y ejecutar un juego basico con Phaser.,pero tambien una intrudccion a JS.

### Introduccion a JS

##### Variables #####

Las variables son un almacen nombrado para guardar datos, que mas tarde seran usados para realizar calculos u operaciones que requieren estos valores.

```javascript
let nombre;
var apellido;

nombre = "Larry";
apellido = "Ellison";

console.log(nombre + " " + apellido);
```

##### Constantes #####

Las constantes son un almacen nombrado para guardatos y que una vez asignado, no podran ser modificados en el codigo.

```javascript
const COLOR = "#C0C0C0";

console.log("El color de fondo definido es " + COLOR);
```

##### NULL y undefinied #####

Cuando se define una variable y no se asigna valor, este tiene un 'valor' nulo o vario; por otro lado, undefined significa "valor no asignado", aun que en terminos practicos ambos son equivalentes.

Es util para saber si una variable tiene valor o no:

```javascript

var nombre;

if(nombre == undefined) {

}

//Tambien equivalente
if(nombre) {

}
```

##### Funciones #####

Una funcion es un fragmento de codigo destinado a realizar una tarea en particular, y que permite la organizacion y reutilizacion del codigo de manera intuitiva.

```javascript

function hola(nombre) {
    console.log("Hola " + nombre);
}

const hola = function(nombre) {
    console.log("Hola " + nombre);
};
hola("Larry");

const hola = (nombre) => {
    console.log("Hola " + nombre);
};
hola("Larry");

//Funciones anonimas
(function() {
    console.log("Funcion auto ejecutada");
})();

(() => {
    console.log("Funcion auto ejecutada");
})();
```

##### Callbacks #####

```javascript
var iterVal = 0;

const datos = ["Tarea A" ,"Tarea B", "Tarea C"];
const whenDone = (taskName) => {
    console.log("Tarea terminada " + taskName);
};

const execTask = (task, callback) => {
    const text = task + " con valor " + iterVal++;
    callback(text);
}

datos.forEach( (valor, indice, array) => {
    execTask(valor, whenDone);
});

```

### Que necesito para iniciar con Phaser?

* Distribucion de phaser
* Instalacion de servidor web
* Como continuo?
* Que sigue ?

### Distribucion de phaser ###

"Instalar" Phaser es muy simple, dirigete a la pagina de [Phaser](https://phaser.io/download/stable) y descarga la version zip de la distribucion.

### Instalacion de servidor web ###

Por que requiero un servidor web?

Un servidor web permite que tu juego sea consultado o ejecutado desde cualquier ubicacion, poniendo a disposicion todos los recursos necesarios sin tener que descargar un archivo instalador.

Es posible usar cualquier servidor web, desde apache hasta esta version simple de un servidor web. 

Para usar esta version, en el directorio "server" encontraras los archivos publicos que tu juego requiere y mas adelante revisaremos.

Para ejecutar este servidor, necesitas tener instalado nodejs, para descargarlo ve a [la pagina de nodejs](https://nodejs.dev/) y descarga la version para tu computadora. Instala nodejs como cualquier otra aplicacion windows.

Una vez instalaste nodejs, abre una ventana de comandos (Menu de Windows -> Ejecutar -> CMD) y teclea lo siguiente:

```bash
node -v
v8.15.1
```

si ves la version despues de ejecutar el comando, tu instalacion ya esta lista.

### Como continuo? ###

Una vez que descargaste el proyecto, que instalaste nodejs , debes ejecutar nodejs para iniciar el servidor de la siguiente forma:

```bash
    cd <directorio descargado>/server
    npm install
    node index.js
```

Esto iniciara el servidor web y permitira que todo lo que esta en el directorio public sea servido al publico, por lo que todos los archivos de tu juego deberan estar en este directorio public.

Deberias ver un mensaje similar a este:

```bash
Server listening on URL: http://localhost:3000
```

si lo logras ver, vas bien hasta este punto. Ve a tu navegador y entra a la direccion http://localhost:3000

### Que sigue ? ###

Sigue las nociones basicas de juegos en [Phaser](https://phaser.io/tutorials/making-your-first-phaser-3-game-spanish/index) para comprender como hacer un mundo e interactuar con sus elementos.